
def approx(f_x, h, dfdx):
    return f_x + h * dfdx

print (" #4 ----------------------------------------------------------------------------\n")

x = -3.0
f_x = 0
dfdxc = -0.5*pow(x,2) - 0.5*f_x
h = 0.5
print("f(-3) = 0")

while x < -1.0:
    print(
    "f({0:.2f} + {2:.3f}) = {1:.3f} + {2:.3f}*{3:.3f} = {4:.3f}".format( 
    round(x,3), round(f_x,3), h, round(dfdxc,3), round(approx(f_x, h, dfdxc),3)
    ))
    x += h
    f_x = approx(f_x, h, dfdxc)
    dfdxc = -0.5*pow(x,2) - 0.5*f_x


print (" \n#5 ----------------------------------------------------------------------------\n")


def _dfdx(x):
    return 0.5 * pow(x,2.0)

x = 1.0
f_x = -3.0
h = 0.5
print(
"f({0}) = {1}".format(x,f_x)
)

while x < 3.0:
    print(
    "f({0:.2f} + {2:.3f}) = {1:.3f} + {2:.3f}*{3:.3f} = {4:.3f}".format( 
    round(x,3), round(f_x,3), h, round(_dfdx(x),3), round(approx(f_x, h, _dfdx(x)),3)
    ))
    f_x = approx(f_x, h, _dfdx(x))
    x += h

print (" \n#1 ----------------------------------------------------------------------------\n")
def approx(f_x, h, dfdx):
    print ("f(x + {h}) = {f_x:.3f} + {h} * {dfdx} = {res:.3f}".format(h=h,f_x=round(f_x,3),dfdx=dfdx,res=round(f_x + h * dfdx,3)))
    return f_x + h * dfdx
    
def solve(f_x_IN, h, dfdx_list):
    f_x = f_x_IN
    
    print (  "f(x) = {0}".format(f_x)  )
    
    for dfdx in dfdx_list:
        f_x = approx(f_x,h,dfdx)
        #print (  "f(x + {h}) = {0}".format(f_x,h=h)  )
        
solve(17, 0.3, [-4, -4.3, -4.2, -4.8])

print (" \n#2 ----------------------------------------------------------------------------\n")

solve(11, 0.3, [1.0, 0.1, 0.1, -0.7])

print (" \n#6 ----------------------------------------------------------------------------\n")

solve(4, 0.3, [-5.0, -4.9, -4.0, -4.7])
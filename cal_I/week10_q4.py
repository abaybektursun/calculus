# Newton's method

print("#1 -------------------------------------------------")

def f(x):
    return -4*pow(x,3)-2*pow(x,2)-2*x-3

def dfdx(x):
    return -12*pow(x,2)-4*x-2

print(
"f({}) = {}".format(-2,f(-2))
)

print(
"f({}) = {}".format(0,f(0))
)

x = []
x.append(-2)

for x_idx in range(1,3):
    x.append( x[x_idx-1] - f(x[x_idx-1])/dfdx(x[x_idx-1]) )
    print ("x[{}] = {}".format(x_idx,x[x_idx]))
 
print("#2 -------------------------------------------------")

def f(x):
    return 3*pow(x,5)-6*pow(x,4)+4*pow(x,3)-4*pow(x,2)-3*x-3

def dfdx(x):
    return 15*pow(x,4)-24*pow(x,3)+12*pow(x,2)-8*x-3

print(
"f({}) = {}".format(0,f(0))
)

print(
"f({}) = {}".format(2,f(2))
)

x = []
x.append(0)

for x_idx in range(1,3):
    x.append( x[x_idx-1] - f(x[x_idx-1])/dfdx(x[x_idx-1]) )
    print ("x[{}] = {}".format(x_idx,x[x_idx]))
    
print("#3 -------------------------------------------------")

def f(x):
    return 5*pow(x,3)-5*x+7

def dfdx(x):
    return 15*pow(x,2)-5

print(
"f({}) = {}".format(-2,f(-2))
)

print(
"f({}) = {}".format(0,f(0))
)

x = []
x.append(-2)

for x_idx in range(1,3):
    x.append( x[x_idx-1] - f(x[x_idx-1])/dfdx(x[x_idx-1]) )
    print ("x[{}] = {}".format(x_idx,x[x_idx]))
 
 
print("#4 -------------------------------------------------")

def f(x):
    return -6*pow(x,5)-5*pow(x,4)+7*pow(x,3)+pow(x,2)-2*x-2

def dfdx(x):
    return -30*pow(x,4)-20*pow(x,3)+21*pow(x,2)+2*x-2

print(
"f({}) = {}".format(-2,f(-2))
)

print(
"f({}) = {}".format(0,f(0))
)

x = []
x.append(-2)

for x_idx in range(1,3):
    x.append( x[x_idx-1] - f(x[x_idx-1])/dfdx(x[x_idx-1]) )
    print ("x[{}] = {}".format(x_idx,x[x_idx]))
 
print("#5 -------------------------------------------------")

def f(x):
    return -1*pow(x,5)+5*pow(x,4)-4*pow(x,3)+6*pow(x,2)-2*x+3

def dfdx(x):
    return -5*pow(x,4)+20*pow(x,3)-12*pow(x,2)+12*pow(x,1)-2

print(
"f({}) = {}".format(4,f(4))
)

print(
"f({}) = {}".format(6,f(6))
)

x = []
x.append(4)

for x_idx in range(1,3):
    x.append( x[x_idx-1] - f(x[x_idx-1])/dfdx(x[x_idx-1]) )
    print ("x[{}] = {}".format(x_idx,x[x_idx]))
 
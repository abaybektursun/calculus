def approx(f_x, h, dfdx):
    return f_x + h * dfdx
    
print (" \n#3 ----------------------------------------------------------\n")

def _dfdx(x):
    return 0.5*x+0.5*f_x

x = 0.0
f_x = 2.0
h = 0.5
print(
"f({0}) = {1}".format(x,f_x)
)

while x < 2.0:
    print(
    "f({0:.2f} + {2:.2f}) = {1:.2f} + {2:.2f}*{3:.2f} = {4:.8f}".format( 
    round(x,2), round(f_x,2), h, round(_dfdx(x),2), round(approx(f_x, h, _dfdx(x)),8)
    ))
    f_x = approx(f_x, h, _dfdx(x))
    x += h
    
print (" \n#5 ----------------------------------------------------------\n")

def f(x):
    return -7*pow(x,5) - 6*pow(x,4) + pow(x,3) + 4*pow(x,2)+4*x+2

def dfdx(x):
    return -35*pow(x,4) - 24*pow(x,3) + 3*pow(x,2) + 8*pow(x,1)+4

print(
"f({}) = {}".format(0,f(0))
)

print(
"f({}) = {}".format(2,f(2))
)

x = []
x.append(0)

for x_idx in range(1,3):
    x.append( x[x_idx-1] - f(x[x_idx-1])/dfdx(x[x_idx-1]) )
    print ("x[{}] = {}".format(x_idx,x[x_idx]))
 
print (" \n#6 ----------------------------------------------------------\n")

def _dfdx(x):
    return 1*x-1*f_x

x = 1.0
f_x = 2.0
h = 0.5
print(
"f({0}) = {1}".format(x,f_x)
)

while x < 3.0:
    print(
    "f({0:.5f} + {2:.5f}) = {1:.5f} + {2:.5f}*{3:.5f} = {4:.5f}".format( 
    round(x,5), round(f_x,5), h, round(_dfdx(x),5), round(approx(f_x, h, _dfdx(x)),5)
    ))
    f_x = approx(f_x, h, _dfdx(x))
    x += h

print (" \n#7 ----------------------------------------------------------\n")

def _dfdx(x):
    return -0.5*x+0.5*f_x

x = -2.0
f_x = -2.0
h = 0.5
print(
"f({0}) = {1}".format(x,f_x)
)

while x < 0.0:
    print(
    "f({0:.5f} + {2:.5f}) = {1:.5f} + {2:.5f}*{3:.5f} = {4:.5f}".format( 
    round(x,5), round(f_x,5), h, round(_dfdx(x),5), round(approx(f_x, h, _dfdx(x)),5)
    ))
    f_x = approx(f_x, h, _dfdx(x))
    x += h
    
print (" \n#8 ----------------------------------------------------------\n")

def f(x):
    return pow(x,5) - 3*pow(x,4)-  3*pow(x,3) +6*pow(x,2) +6*x -6

def dfdx(x):
    return 5*pow(x,4) - 12*pow(x,3)-  9*pow(x,2) +12*pow(x,1) +6

print(
"f({}) = {}".format(2,f(2))
)

print(
"f({}) = {}".format(4,f(4))
)

x = []
x.append(2)

for x_idx in range(1,3):
    x.append( x[x_idx-1] - f(x[x_idx-1])/dfdx(x[x_idx-1]) )
    print ("x[{}] = {}".format(x_idx,x[x_idx]))
 
print (" \n#9 ----------------------------------------------------------\n")

def f(x):
    return -2*pow(x,3) -4*pow(x,2) +5*x -6

def dfdx(x):
    return -6*pow(x,2) -8*x + 5 
    
print(
"f({}) = {}".format(-4,f(-4))
)

print(
"f({}) = {}".format(-2,f(-2))
)

x = []
x.append(-4)

for x_idx in range(1,3):
    x.append( x[x_idx-1] - f(x[x_idx-1])/dfdx(x[x_idx-1]) )
    print ("x[{}] = {}".format(x_idx,x[x_idx]))
 
    

# 1
def f(x):
    return (-5*x**4)/2 + (4*x**3)/3 - 3*x**2 + 5*x
    
print( f(12) - f(10) ) 
#-25991.333333333332

# 2
def f(x):
    return (3*x**4)/4 + 2*x**3 - (7*x**2)/2 - 6*x
    
print ( f(5)-f(2) )

# 3
def f(x):
    return (-10*x**4)/4 + x**3 - 2*x**2 - 9*x
    
print ( f(7)-f(2) )
import numpy as np
import matplotlib.pyplot as plt
# Question 2 -------------------------------------
for n in range(1,6):
	print('1/' + str(n) + '=' + str(1/n) )
print()
# Result
"""
1/1=1.0
1/2=0.5
1/3=0.3333333333333333
1/4=0.25
1/5=0.2
"""
	
# Question 4 and 6 -------------------------------------
# Finite sum is a sum of a squence that can be computed in finite amount of time
#Example: sum of first 4 real numbers
seq = [1, 2, 3, 4]
print( sum(seq) )
print()
# Result
"""
10
"""

# Question 12 -------------------------------------
for n in range(1,5):
	print('2+(-1)^{} = {}'.format(n, 2 + (-1)^n) )
print()
# Result
"""
2+(-1)^1 = 0
2+(-1)^2 = 3
2+(-1)^3 = 2
2+(-1)^4 = 5
"""

# Question 14 -------------------------------------
for n in range(1,5):
	print('2*{0}^2-3*{0}+1 = {1}'.format(n,2*n^2-3*n+1 ))
print()
# Result
"""
2*1^2-3*1+1 = 2
2*2^2-3*2+1 = -7
2*3^2-3*3+1 = -4
2*4^2-3*4+1 = -1
"""

# Question 16 -------------------------------------
for n in range(1,5):
	print('2*{0}^2 - 3*{0} + 1 = {1}'.format(n, 2*n^2 - 3*n + 1) )
print()
# Result
"""
2*1^2 - 3*1 + 1 = 2
2*2^2 - 3*2 + 1 = -7
2*3^2 - 3*3 + 1 = -4
2*4^2 - 3*4 + 1 = -1
"""

# Question 18 -------------------------------------
def a(n):
	if n == 1:
		return 32
	return a(n-1)/2
	
for n in range(1,5):
	print('a({0}) = {1}'.format(n, a(n)) )
print()
# Result
"""
a(1) = 32
a(2) = 16.0
a(3) = 8.0
a(4) = 4.0
"""

# Question 20 -------------------------------------
def a(n):
	if n == 1:
		return 1
	return a(n-1)^2 - 1
	
for n in range(1,5):
	print('a({0}) = {1}'.format(n, a(n)) )
print()
# Result
"""
a(1) = 1
a(2) = 0
a(3) = 1
a(4) = 0
"""

# Question 24 -------------------------------------
seq = [1, -2, 3, -4, 5]
# Geomteric Mean Analisys
for n in range(0,len(seq)-2):
	print (np.sqrt(seq[n] * seq[n+2]))
print()
# Result
"""
1.73205080757
2.82842712475
3.87298334621
"""

# Arithemtic Mean Analisys
for n in range(0,len(seq)-2):
	print ((seq[n]  + seq[n+2]) / 2)
print()
# Result
"""
2.0
-3.0
4.0
"""

# Arithemtic means are telling us that there is an aritmetic progression
# We could assume that sequence changes like: +2, -3, +4, -5 .... flactuating sign
# Explicit picewise function:
def a(n):
	if n % 2:
		return n
	if not n % 2:
		return -1 * n

for n in range(1, 10):
	print(a(n))
print()
# Result
"""
1
-2
3
-4
5
-6
7
-8
9
"""
# We also could write recursive version: a_{n} = a_{n-2}/|a_{n-2}| * a_{n} where a_{1} = 1 and a_{2} = -2
def a(n):
	if n == 1:
		return 1
	if n == 2:
		return -2
	an_2 = a(n-2)
	return (an_2/np.absolute(an_2)) * n

for n in range (1,10):
	print(a(n))
print()
# Result
"""
1
-2
3.0
-4.0
5.0
-6.0
7.0
-8.0
9.0
"""

# Question 26 -------------------------------------
seq = [2, 5, 8, 11]
# Geomteric Mean Analisys
for n in range(0,len(seq)-2):
	print (np.sqrt(seq[n] * seq[n+2]))
print()
# Result
"""
4.0
7.4161984871
"""

# Arithemtic Mean Analisys
for n in range(0,len(seq)-2):
	print ((seq[n]  + seq[n+2]) / 2)
print()
# Result
"""
5.0
8.0
"""

# Again, arithemtic means are telling us a story
# Explicit function: 2 + 3(n) 
def a(n):
	return 2 + 3*n
for n in range (0,9):
	print(a(n))
print()
# Result
"""
2
5
8
11
14
17
20
23
26
"""
#Recursive function: mathematically, input starts from 0 
# Here is input form 1 to make function easier
def a(n):
	if n == 1:
		return 2
	return a(n-1) + 3
	
for n in range (1,10):
	print(a(n))
print()
# Result
"""
2
5
8
11
14
17
20
23
26
"""

# Question 48 -------------------------------------
ax = plt.subplot(111)

t = np.arange(2.0, 100.0, 1.0)
s = (np.power(t,2)/(np.power(t,2) -1) )
line, = plt.plot(t, s, lw=2)

plt.show()

#Looking at the graph we can conclude (assume) that the sequence converges to 1


print(s[0:4])
print()
#Result
""""
[ 1.3	1.125	1.07	1.04]
"""


# Home Repository: https://gitlab.com/abaybektursun/calculus



# Question 50 -------------------------------------
def a(n):
    if n == 0:
        return 1
    return (1/4)*a(n-1)-3
    
# Plot
ax = plt.subplot(111)

t = np.arange(0.0, 100.0, 1.0)
s = []
for n in t:
    s.append(a(n))

line, = plt.plot(t, s, lw=2)

plt.show()

for n in range(0,12):
    print(a(n))
print()

# Result:
"""
1
-2.75
-3.6875
-3.921875
-3.980468.....
-3.995117.....
-3.998779.....
-3.999694.....
-3.999923.....
-3.999980.....
-3.999995.....
-3.999998.....
"""

# Looking at the plot and the numbers we can conclude (assume) sequence in sonverging to -4



# Question 54 -------------------------------------
def a(n):
    if n == 0:
        return 1
    return np.sqrt(1+a(n-1))

# Plot
ax = plt.subplot(111)

t = np.arange(0.0, 100.0, 1.0)
s = []
for n in t:
    s.append(a(n))

line, = plt.plot(t, s, lw=2)

plt.show()


for n in range(0,120):
    print(a(n))
print()

# Result:
"""
1
1.41421356237
1.55377397403
1.59805318248
1.61184775413
1.61612120651
1.61744279853
1.61785129061
1.61797753093
1.61801654223
1.61802859747
1.61803232275
"""

# Looking at the plot and the numbers we can conclude (assume) sequence in converging to 1.618

# Question 56 -------------------------------------
base_case = 10
adjust = 0.9
def a(n):
    if n == 0:
        return base_case
    return a(n-1)*adjust
    
for n in range(0,10):
    print(a(n))
print()

# Result
"""
10
9.0
8.1
7.29
6.561
5.904900...
5.314410...
4.782969...
4.304672...
3.874204...
"""

# Recursive: a(n) = a(n-1)*0.9
#Explicit formula:?

# Question 58 -------------------------------------
base_case = 20
adjust = 0.75
def a(n):
    if n == 0:
        return base_case
    return a(n-1)*adjust
    
for n in range(0,10):
    print(a(n))
print()

# Result
"""
20
15.0
11.25
8.4375
6.3281...
4.7460...
3.5595...
2.6696...
2.0022...
1.5016...
"""

# Recursive: a(n) = a(n-1)*0.75; a(0) = 20
#Explicit formula:?
